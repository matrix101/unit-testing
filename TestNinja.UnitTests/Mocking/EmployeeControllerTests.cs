﻿using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using TestNinja.Mocking;
using Xunit;

namespace TestNinja.UnitTests.Mocking
{
    public class EmployeeControllerTests
    {
        [Fact]
        public void DeleteEmployee_WhenCalled_DeleteTheEmployee()
        {
            var storage = new Mock<IEmployeeStorage>();
            var controller = new EmployeeController(storage.Object);

            controller.DeleteEmployee(1);

            storage.Verify(s => s.DeleteEmployee(1));
        }
    }
}

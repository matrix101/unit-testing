﻿using Moq;
using TestNinja.Mocking;
using Xunit;
using static TestNinja.Mocking.OrderService;

namespace TestNinja.UnitTests.Mocking
{
    public class OrderServiceTests
    {
        [Fact]
        public void PlaceOrder_WhenCalled_StoreOrder()
        {
            var storage = new Mock<IStorage>();
            var service = new OrderService(storage.Object);

            var order = new Order();
            service.PlaceOrder(order);

            storage.Verify(x => x.Store(order));
        }
    }
}

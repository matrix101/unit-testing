﻿using Moq;
using Shouldly;
using System.Net;
using TestNinja.Mocking;
using Xunit;

namespace TestNinja.UnitTests.Mocking
{
    public class InstallerHelperTests
    {
        private readonly Mock<IFileDownloader> _fileDownloader;
        private readonly InstallerHelper _installerHelper;

        public InstallerHelperTests()
        {
            _fileDownloader = new Mock<IFileDownloader>();
            _installerHelper = new InstallerHelper(_fileDownloader.Object);
        }

        [Fact]
        public void DownloadInstaller_DownloadFails_ReturnsFalse()
        {
            _fileDownloader.Setup(x =>
                x.DownloadFile(It.IsAny<string>(), It.IsAny<string>()))
                .Throws<WebException>();

            var result = _installerHelper.DownloadInstaller("customer", "installer");

            result.ShouldBeFalse();
        }

        [Fact]
        public void DownloadInstaller_DownloadComplete_ReturnsTrue()
        {
            _fileDownloader.Setup(x =>
                x.DownloadFile(It.IsAny<string>(), It.IsAny<string>()));

            var result = _installerHelper.DownloadInstaller("customer", "installer");

            result.ShouldBeTrue();
        }
    }
}

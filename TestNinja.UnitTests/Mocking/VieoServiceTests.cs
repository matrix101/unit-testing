﻿using Moq;
using System.Collections.Generic;
using TestNinja.Mocking;
using Xunit;

namespace TestNinja.UnitTests
{
    public class VieoServiceTests
    {
        private readonly VideoService _videoService;
        private readonly Mock<IFileReader> _fileReader;
        private readonly Mock<IVideoRepository> _videoRepository;
        public VieoServiceTests()
        {
            _fileReader = new Mock<IFileReader>();
            _videoRepository = new Mock<IVideoRepository>();
            _fileReader.Setup(x => x.Read("video.json")).Returns("");

            _videoService = new VideoService(_fileReader.Object, _videoRepository.Object);

        }

        [Fact]
        public void ReadVideoTitle_EmptyFile_ReturnError()
        {
            var result = _videoService.ReadVideoTitle();

            Shouldly.ShouldBeStringTestExtensions.ShouldContain(result, "error", Shouldly.Case.Insensitive);
        }

        [Fact]
        public void GetUnprocessedVideoAsCsv_AllVideoAreProcessed_ReturnsEmptyString()
        {
            _videoRepository.Setup(r => r.GetUnprocessedVideos()).Returns(new List<Video>());
            var result = _videoService.GetUnrpocessedVideoAsCsv();

            Shouldly.ShouldBeStringTestExtensions.ShouldBe(result, "");
        }

        [Fact]
        public void GetUnprocessedVideoAsCsv_AFewUnprocessedVideo_ReturnsAStringWithIdOfUnprocessedVideos()
        {
            _videoRepository.Setup(r => r.GetUnprocessedVideos()).Returns(new List<Video>
            {
                new Video { Id = 1},
                new Video { Id = 5}
            });
            var result = _videoService.GetUnrpocessedVideoAsCsv();

            Shouldly.ShouldBeStringTestExtensions.ShouldBe(result, "1,5");
        }
    }
}

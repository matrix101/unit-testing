﻿using Shouldly;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace TestNinja.UnitTests
{
    public class ErrorLoggerTests
    {

        [Fact]
        public void Log_WhenCalled_SetTheLastErrorProperty()
        {
            var logger = new ErrorLogger();

            logger.Log("a");

            logger.LastError.ShouldBe("a");
        }

        [Theory]
        [InlineData("")]
        [InlineData(" ")]
        [InlineData(null)]
        public void Log_InvalidError_ThrowArgumentNulLException(string error)
        {
            var logger = new ErrorLogger();

            ShouldThrowExtensions.ShouldThrow<ArgumentNullException>(() => logger.Log(error));
        }

        [Fact]
        public void Log_ValidError_RaiseErrorLoggedEvent()
        {
            var logger = new ErrorLogger();

            var id = Guid.Empty;
            logger.ErrorLogged += (sender, args) =>
            {
                id = args;
            };

            logger.Log("a");

            id.ShouldNotBe(Guid.Empty);
        }
    }
}

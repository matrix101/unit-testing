using Shouldly;
using System;
using Xunit;

namespace TestNinja.UnitTests
{
    public class ResarvationTests
    {
        [Fact]
        public void CanBeCancelledBy_UserIsAdmin_ReturnsTrue()
        {
            var reservation = new Reservation();

            var result = reservation.CanBeCancelledBy(new User
            {
                IsAdmin = true
            });

            result.ShouldBeTrue();
        }

        [Fact]
        public void CanBeCancelledBy_SameUserCancellingTheReservation_ReturnsTrue()
        {
            var user = new User
            {
                Name = "Test"
            };
            var reservation = new Reservation
            {
                MadeBy = user
            };

            var result = reservation.CanBeCancelledBy(user);

            result.ShouldBeTrue();
        }

        [Fact]
        public void CanBeCancelledBy_AnotherUserCancellingTheReservation_ReturnsFalse()
        {
            var reservation = new Reservation
            {
                MadeBy = new User()
            };

            var result = reservation.CanBeCancelledBy(new User());

            result.ShouldBeFalse();
        }
    }
}

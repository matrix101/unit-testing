using Xunit;

namespace TestNinja.UnitTests
{
    public class FizzBuzzTests
    {

        [Fact]
        public void GetOutput_InputIsDivisibleBy3And5_ReturnFizzBuzz()
        {

            var result = FizzBuzz.GetOutput(15);

            Shouldly.ShouldBeStringTestExtensions.ShouldBe(result, "FizzBuzz");
        }
        [Fact]
        public void GetOutput_InputIsDivisibleBy3Only_ReturnFizz()
        {
            var result = FizzBuzz.GetOutput(3);

            Shouldly.ShouldBeStringTestExtensions.ShouldBe(result, "Fizz");

        }

        [Fact]
        public void GetOutput_InputIsDivisibleBy5Only_ReturnBuzz()
        {
            var result = FizzBuzz.GetOutput(5);

            Shouldly.ShouldBeStringTestExtensions.ShouldBe(result, "Buzz");

        }

        [Fact]
        public void GetOutput_InputIsNotDivisibleBy3And5_ReturnSameNumber()
        {
            var result = FizzBuzz.GetOutput(1);

            Shouldly.ShouldBeStringTestExtensions.ShouldBe(result, "1");
        }
    }
}

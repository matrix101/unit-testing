﻿using Shouldly;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace TestNinja.UnitTests
{
    public class MathTests
    {
        private Math _math = new Math();
        [Fact]
        public void GetOddNumber_LimitIsGreaterThanZero_ReturnOddNumbersUpToLimit()
        {
            var result = _math.GetOddNumbers(5);

            //result.ShouldNotBeEmpty();

            //result.ShouldContain(1);
            //result.ShouldContain(3);
            //result.ShouldContain(5);

            result.ShouldBe(new[] { 1, 3, 5 });

            result.ShouldBeInOrder(SortDirection.Ascending);
            result.ShouldBeUnique();
        }
    }
}

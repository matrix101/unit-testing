﻿using Shouldly;

namespace TestNinja.UnitTests
{
    class HtmlFromatterTests
    {
        public void FormatAsBold_WhenCalled_ShouldEncloseTheStringWithStrongElement()
        {
            var formatter = new HtmlFormatter();

            var result = formatter.FormatAsBold("abc");

            ShouldBeStringTestExtensions.ShouldBe(result, "<strong>abc</strong>", StringCompareShould.IgnoreCase);
        }
    }
}

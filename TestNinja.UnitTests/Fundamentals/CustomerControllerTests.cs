﻿using Shouldly;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using static TestNinja.CustomController;

namespace TestNinja.UnitTests
{
    public class CustomerControllerTests
    {
        [Fact]
        public void GetCustomer_IdIsZero_ReturnNotFound()
        {
            var controller = new CustomController();

            var result = controller.GetCustomer(0);

            result.ShouldBeOfType<NotFound>();
        }

        [Fact]
        public void GetCustomer_IdIsNotZero_ReturnOk()
        {
            var controller = new CustomController();

            var result = controller.GetCustomer(20);

            result.ShouldBeOfType<Ok>();
        }
    }
}

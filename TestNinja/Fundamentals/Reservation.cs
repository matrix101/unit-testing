﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestNinja
{
    public class Reservation
    {
        public User MadeBy { get; set; }
        
        public bool CanBeCancelledBy(User user)
        {
            return (user.IsAdmin || MadeBy == user);
        }
    }
}

﻿namespace TestNinja
{
    public class User
    {
        public string Name { get; set; }
        public string Surname { get; set; }

        public bool IsAdmin { get; set; }
    }
}
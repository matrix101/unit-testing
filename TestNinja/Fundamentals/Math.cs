﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestNinja
{
    public class Math
    {
        public IEnumerable<int> GetOddNumbers(int limit)
        {
            for (int i = 0; i <= limit; i++)
                if (i % 2 != 0)
                    yield return i;

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestNinja
{
    public class HtmlFormatter
    {
        public string FormatAsBold(string value)
        {
            return $"<strong>{value}</strong>";
        }
    }
}

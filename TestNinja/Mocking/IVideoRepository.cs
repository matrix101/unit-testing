﻿using System.Collections.Generic;

namespace TestNinja.Mocking
{

    public interface IVideoRepository
    {
        public IEnumerable<Video> GetUnprocessedVideos();
    }
}
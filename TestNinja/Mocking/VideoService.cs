﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestNinja.Mocking
{
    public class VideoService
    {
        private readonly IFileReader fileReader;
        private readonly IVideoRepository videoRepository;

        public VideoService(IFileReader fileReader, IVideoRepository videoRepository)
        {
            this.fileReader = fileReader;
            this.videoRepository = videoRepository;
        }
        public string ReadVideoTitle()
        {
            var str = fileReader.Read("video.json");
            var video = JsonConvert.DeserializeObject<Video>(str);
            if (video is null)
                return "Error parsing the video.";
            return video.Title;
        }

        public string GetUnrpocessedVideoAsCsv()
        {
            var videoIds = new List<int>();

            var videos = videoRepository.GetUnprocessedVideos();

            foreach (var v in videos)
                videoIds.Add(v.Id);

            return String.Join(",", videoIds);
        }
    }

    internal class VideoContext :IDisposable
    {
        public IEnumerable<Video> Videos { get; set; }

        public void Dispose()
        {
        }
    }

    public class Video
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public bool IsProcessed { get; set; }
    }
}

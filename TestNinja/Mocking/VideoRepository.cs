﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestNinja.Mocking
{
    class VideoRepository
    {
        public IEnumerable<Video> GetUnprocessedVideos()
        {
            using var context = new VideoContext();
            return (from video in context.Videos
                    where !video.IsProcessed
                    select video).ToList();
        }
    }
}

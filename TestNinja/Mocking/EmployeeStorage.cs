﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestNinja.Mocking
{
    public class EmployeeStorage : IEmployeeStorage
    {
        private EmployeeContext _db;
        public EmployeeStorage()
        {
            _db = new EmployeeContext();
        }
        public void DeleteEmployee(int id)
        {
            var employee = _db.Employees.Single(x => x.Id == id);
            if (employee is null)
                return;

            _db.Employees.Remove(employee);
            _db.SaveChanges();
        }
    }
}

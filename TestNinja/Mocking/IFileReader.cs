﻿namespace TestNinja.Mocking
{
    public interface IFileReader
    {
        public string Read(string path);
    }
}
